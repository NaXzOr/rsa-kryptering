﻿using System;
using System.IO;

namespace rsa_project
{
    public interface IKeyRetriever
    {
        T Execute<T>(string path);
    }

    public class KeyRetriever : IKeyRetriever
    {
        private readonly IReadFile _fileReader;

        public KeyRetriever(IReadFile fileReader)
        {
            _fileReader = fileReader;
        }

        public T Execute<T>(string path)
        {
            var fileContents = ReadFileContents(path);
            var key = Activator.CreateInstance(typeof(T), new object[] { fileContents });

            return (T)key;
        }

        private byte[] ReadFileContents(string path)
        {
            return File.ReadAllBytes(path);
        }
    }
}