﻿using EasyEncrypt.RSA;

namespace rsa_project
{
    public interface IKeyGenerator
    {
        void Execute(string pathToKey);
    }
    public class KeyGenerator : IKeyGenerator
    {
        private readonly IWriteFile _writeFile;

        public KeyGenerator(IWriteFile writeFile)
        {
            _writeFile = writeFile;
        }
        public void Execute(string pathToKey)
        {
            var key = EasyRSA.CreateKey();
            var privateKey = key.PrivateKey;
            _writeFile.Execute(pathToKey+"privateKey.txt",privateKey);

            var publicKey = key.PublicKey;
            _writeFile.Execute(pathToKey+"publicKey.txt",publicKey);

        }
    }
}