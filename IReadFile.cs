﻿using System;
using System.IO;

namespace rsa_project
{
    public interface IReadFile
    {
        string Execute(string pathToFile);
    }

    public class ReadFile : IReadFile
    {
        private readonly ICheckIfFileExists _checkIfFileExists;

        public ReadFile(ICheckIfFileExists checkIfFileExists)
        {
            _checkIfFileExists = checkIfFileExists;
        }
        public string Execute(string pathToFile)
        {
            return _checkIfFileExists.Execute(pathToFile) ? File.ReadAllText(pathToFile) : "";
        }
    }

    public interface ICheckIfFileExists
    {
        bool Execute(string pathToFile);
    }
    public class CheckIfFileExists : ICheckIfFileExists
    {
        public bool Execute(string pathToFile)
        {
            return File.Exists(pathToFile);
        }
    }
}