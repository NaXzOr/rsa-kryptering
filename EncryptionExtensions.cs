﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using EasyEncrypt.RSA;

namespace rsa_project
{
    public static class EncryptionExtensions
    {
        public static string Encrypt(this string str, PublicRSA key)
        {
            return key.Encrypt(str);
        }

        public static string Decrypt(this string str, PrivateRSA key)
        {
            return key.Decrypt(str);
        }

        public static void ToConsole(this string str)
        {
            Console.WriteLine(str);
        }
    }
}
