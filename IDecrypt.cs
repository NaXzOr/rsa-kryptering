﻿using System;
using System.Collections.Generic;
using System.Text;
using EasyEncrypt.RSA;

namespace rsa_project
{
    public interface IDecrypt
    {
        string Execute(string str);
    }

    public class Decrypt: IDecrypt
    {
        private readonly PrivateRSA _privateRsa;

        public Decrypt(PrivateRSA privateRsa)
        {
            _privateRsa = privateRsa;
        }

        public string Execute(string str )
        {
            return _privateRsa.Decrypt(str);
        }
    }
}
