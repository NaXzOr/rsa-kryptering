﻿using System;
using System.Collections.Generic;
using System.Text;
using EasyEncrypt.RSA;

namespace rsa_project
{
    public interface IEncrypt
    {
        string Execute(string str);
    }

    public class Encrypt : IEncrypt
    {
        private readonly PublicRSA _publicRsa;

        public Encrypt(PublicRSA publicRsa)
        {
            _publicRsa = publicRsa;
        }
        public string Execute(string str)
        {
            return _publicRsa.Encrypt(str);
        }
    }
}
