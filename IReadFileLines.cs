﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace rsa_project
{
    public interface IReadFileLines
    {
        List<string> Execute(string pathToFile);
    }

    public class ReadFileLines : IReadFileLines
    {
        public List<string> Execute(string pathToFile)
        {
            return File.ReadAllLines(pathToFile).ToList();
        }
    }
}