﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using EasyEncrypt.RSA;

namespace rsa_project
{
    public class Program
    {
        static void Main(string[] args)
        {
            var path = Directory.GetCurrentDirectory().Split(Path.DirectorySeparatorChar);
            var pathList = path.ToList().TakeWhile(directoryName => directoryName != "bin").ToArray();
            var projectDirectory = string.Join(Path.DirectorySeparatorChar, pathList);
            var projectDirectoryWithTrailingPathSeperator = $"{projectDirectory}{Path.DirectorySeparatorChar}";
            var pathToEncryptedFile = $"{projectDirectoryWithTrailingPathSeperator}encrypted.txt";
            var pathToDecryptedFile = $"{projectDirectoryWithTrailingPathSeperator}decrypted.txt";
            var keysDirectory = projectDirectoryWithTrailingPathSeperator;
            var pathToPrivateKey = projectDirectoryWithTrailingPathSeperator + "privateKey.txt";
            var pathToPublicKey = projectDirectoryWithTrailingPathSeperator + "publicKey.txt";

            ICheckIfFileExists fileExistChecker = new CheckIfFileExists();
            IReadFile readFile = new ReadFile(fileExistChecker);
            IReadFileLines fileLinesReader = new ReadFileLines();
            IWriteFile writeFile = new WriteFile();
            IKeyRetriever keyRetriever = new KeyRetriever(readFile);
            IKeyGenerator keyGenerator = new KeyGenerator(writeFile);
            PublicRSA RSAPublic;
            PrivateRSA RSAPrivate;

            if (!fileExistChecker.Execute(pathToPrivateKey) && !fileExistChecker.Execute(pathToPublicKey))
                keyGenerator.Execute(keysDirectory);

            RSAPrivate = keyRetriever.Execute<PrivateRSA>(pathToPrivateKey);
            RSAPublic = keyRetriever.Execute<PublicRSA>(pathToPublicKey);


            IDecrypt decryptor = new Decrypt(RSAPrivate);
            IEncrypt encryptor = new Encrypt(RSAPublic);

            while (true)
            {
                "[1] Krypter det du skriver?".ToConsole();
                "[2] Kryptere en selvvalgt streng eller en fil?".ToConsole();
                "[3] Dekryptere encrypted.txt".ToConsole();
                "[4] Afslutte".ToConsole();
                var userChosenArgument = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                switch (userChosenArgument)
                {
                    case 1:
                        "Indtast det du gerne vil have krypteret".ToConsole();
                        var stringToEncrypt = Console.ReadLine();

                        var encrypted = encryptor.Execute(stringToEncrypt);
                        var decrypted = decryptor.Execute(encrypted);
                        $"krypteret streng: {encrypted}".ToConsole();
                        $"dekrypteret streng: {decrypted}".ToConsole();
                        Console.ReadLine();
                        break;
                    case 2:
                        var filesAllowed = Directory.GetFiles(projectDirectory).ToList();
                        "du skal vælge en fil".ToConsole();
                        var allowedFiles = new List<FileMatch>();
                        for (int i = 0; i < filesAllowed.Count; i++)
                        {
                            $"[{i}]: {filesAllowed[i]}".ToConsole();
                            allowedFiles.Add(new FileMatch(i, filesAllowed[i]));
                        }
                        var chosenFile = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                        if (allowedFiles.Any(fileMatch => fileMatch.Id == chosenFile))
                        {
                            var fileContents = fileLinesReader.Execute(allowedFiles[chosenFile].PathToFile);
                            var encryptedLines = fileContents.Select(line => encryptor.Execute(line)).ToList();
                            writeFile.Execute(pathToEncryptedFile, encryptedLines);
                        }
                        break;
                    case 3:
                        var lines = fileLinesReader.Execute(pathToEncryptedFile);
                        var decryptedLines = lines.Select(l => decryptor.Execute(l));
                        decryptedLines.ToList().ForEach(d => d.ToConsole());
                        writeFile.Execute(pathToDecryptedFile, decryptedLines);
                        break;
                    case 4:
                        "Farvel".ToConsole();
                        Environment.Exit(0);
                        break;
                    default:
                        "Du skal vælge et tal".ToConsole();
                        Environment.Exit(-1);
                        break;
                }

            }

        }
    }
}
