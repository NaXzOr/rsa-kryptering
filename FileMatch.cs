﻿using System;
using System.Collections.Generic;
using System.Text;

namespace rsa_project
{
    public class FileMatch
    {
        public int Id { get; }
        public string PathToFile { get; }

        public FileMatch(int id, string pathToFile)
        {
            Id = id;
            PathToFile = pathToFile;
        }
    }
}
