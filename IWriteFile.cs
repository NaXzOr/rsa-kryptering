﻿using System.Collections.Generic;
using System.IO;

namespace rsa_project
{
    public interface IWriteFile
    {
        void Execute(string path, IEnumerable<string> strArr);
        void Execute(string path, string str);
        void Execute(string path, byte[] bytes);
    }

    public class WriteFile : IWriteFile
    {
        public void Execute(string path, IEnumerable<string> strArr)
        {
            File.WriteAllLines(path, strArr);
        }
        public void Execute(string path, string str)
        {
            File.WriteAllText(path, str);
        }

        public void Execute(string path, byte[] bytes)
        {
            File.WriteAllBytes(path, bytes);
        }
    }
}